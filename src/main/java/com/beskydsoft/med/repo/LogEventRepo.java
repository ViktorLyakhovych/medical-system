package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.log.activity.LogEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogEventRepo extends CrudRepository<LogEvent, Long> {

    Iterable<LogEvent> findAllByPatient(Patient patient);

}
