package com.beskydsoft.med.controllers;

import com.beskydsoft.med.entity.data.gadget.uploads.UploadedFile;
import com.beskydsoft.med.service.PatientService;
import com.beskydsoft.med.service.UploadEventService;
import com.beskydsoft.med.service.UploadFileEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class UploadEventController {
    @Autowired
    UploadFileEventService uploadFileEventService;
    @Autowired
    PatientService patientService;
    @Autowired
    UploadEventService uploadEventService;


    @GetMapping("/upload/view/{viewItem}/{uploadFileEventId}")
    public String viewUploadPhotosSelectedById(
            @PathVariable Long uploadFileEventId,
            @PathVariable String viewItem,
            Model model){
        fillModel(model, uploadFileEventId);
        //upper first letter of viewItem and append to "patient" to gen view name
        return "patient" + viewItem.substring(0,1).toUpperCase() + viewItem.substring(1);
    }


    private void fillModel(Model model, Long uploadFileEventId) {
        UploadedFile[] uploadedFiles =
                uploadFileEventService
                .getUploadFileEventById(uploadFileEventId)
                .getUploadedFilesArray();
        model.addAttribute("patients", patientService.getAll()); //need for side online menu
        model.addAttribute("uploadedFiles", uploadedFiles);
    }


    @GetMapping(value = "/patient/{patientId}/upload/del/{id}")
    public String deleteUploadEventById(
            @PathVariable Long id,
            @PathVariable Long patientId
            ){
        uploadEventService.delUploadEventById(id);
        return "redirect:/patient/visits/get/" + patientId;
    }
}
