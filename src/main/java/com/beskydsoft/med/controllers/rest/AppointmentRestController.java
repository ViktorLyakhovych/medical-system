package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.medcard.MedCard;
import com.beskydsoft.med.entity.medcard.Visit;
import com.beskydsoft.med.entity.medcard.history.Action;
import com.beskydsoft.med.entity.medcard.history.appointment.Appointment;
import com.beskydsoft.med.entity.medcard.history.appointment.AppointmentTaking;
import com.beskydsoft.med.json.AppointmentTakingJson;
import com.beskydsoft.med.service.ActionService;
import com.beskydsoft.med.service.AppointmentService;
import com.beskydsoft.med.service.PatientService;
import com.beskydsoft.med.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@RestController
public class AppointmentRestController {
    @Autowired
    PatientService patientService;
    @Autowired
    AppointmentService appointmentService;
    @Autowired
    VisitService visitService;
    @Autowired
    ActionService actionService;

    @GetMapping("/android/patient/{patientId}/appointment/get")
    public List<Appointment> getAppointmentsGroupByPatientId(@PathVariable Long patientId) {
        Patient patient = patientService.getPatient(patientId);
        MedCard medCard = patient.getMedCard();
        Set<Visit> visits = medCard.getVisits();

        List<Appointment> appointments = new ArrayList<>();
        Set<Action> actions = new HashSet<>();
        try {
            for (Visit v : visits) actions.addAll(v.getActions());
        } catch (Exception e) {

        }
        for (Action a : actions) appointments.addAll(a.getAppointment());

        return appointments;
    }

    @PostMapping("/android/patient/add/appointment/taking")
    public @ResponseBody int appointmentTaking(
            @RequestBody List<AppointmentTakingJson> appointmentTakingJsons
    ) {
        Long result = 0L; //count of appointmentTaking to return on android

        for (AppointmentTakingJson appointmentTakingJson: appointmentTakingJsons) {
            AppointmentTaking appointmentTaking = new AppointmentTaking();

            appointmentTaking.setTakingDate(appointmentTakingJson.getTakingDate());
//            appointmentTaking.setTakingDate(LocalDateTime.now());
            appointmentTaking.setAppointment(appointmentService
                            .getAppointment(appointmentTakingJson.getAppointmentId()));

            appointmentService.addAppointmentTaking(appointmentTaking);
            result = appointmentTaking.getAppointment().getId();
        }

      return appointmentService.getAppointmentTakingsCount(result);
    }

}
