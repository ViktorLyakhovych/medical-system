CREATE DATABASE `monimed` CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'user'@'localhost' IDENTIFIED BY 'user_pass';
GRANT ALL ON monimed.* TO 'user'@'localhost';
FLUSH PRIVILEGES;
