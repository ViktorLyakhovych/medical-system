package com.beskydsoft.med.entity.medcard;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Passport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //дата заповнення
    @Column(name = "fillDate")
    private LocalDate fillDate;

    //стать
    @Column(name = "sex")
    private String sex;

    //освіта
    @Column(name = "education")
    private String education;

    // сімейний стан
    @Column(name = "maritalStatus")
    private String maritalStatus;

    // інвалідність
    @Column(name = "invalidity")
    private String invalidity;

    // захворювання у членів сім'ї
    @Column(name = "familyMembersSick")
    private String familyMembersSick;

    // умови проживання
    @Column(name = "accommodation")
    private String accommodation;

    // травми
    @Column(name = "injuries")
    private String injuries;

    // операції
    @Column(name = "operations")
    private String operations;

    // перебування в місцях позбавлення волі
    @Column(name = "prison")
    private String prison;

    // зворотній зв'язок на медичну карту
    @OneToOne(mappedBy = "passport")
    @JsonBackReference
    private MedCard medCard;
}
