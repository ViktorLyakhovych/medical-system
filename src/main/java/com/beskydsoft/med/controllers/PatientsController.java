package com.beskydsoft.med.controllers;

import com.beskydsoft.med.service.PatientService;
import com.beskydsoft.med.service.UploadEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/patients")
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class PatientsController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private UploadEventService uploadEventService;

    @GetMapping
    public String patients(Model model) {
        model.addAttribute("patients", patientService.getAll());
        return "patients";
    }
}
