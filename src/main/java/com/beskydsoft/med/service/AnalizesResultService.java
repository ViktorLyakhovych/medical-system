package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesAssignment;
import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesResult;
import com.beskydsoft.med.repo.AnalizesResultRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnalizesResultService {
    @Autowired
    AnalizesResultRepo analizesResultRepo;

    @Autowired
    PatientService patientService;

    @Autowired
    AnalizesAssignmentService analizesAssignmentService;

    public List<AnalizesResult> getAllAnalizesResultByPatient(Long patientId) {
        List<AnalizesAssignment> analizesAssignments = analizesAssignmentService.getAllByPatient(patientId);
        List<AnalizesResult> analizesResults = new ArrayList<>();
        for (AnalizesAssignment aa: analizesAssignments) {
            analizesResults.add(aa.getAnalizesResult());
        }
        return analizesResults;
    }

}
