$( document ).ready(function() {
    document.getElementById("tab_visits").click();
});

// ====== show/hide "new visit" form ==============================================
$('#add_visit').click(function() {
    $('#new_visit').toggle({
        "opacity":"0",
        "display":"block",
    }).show().animate({opacity:1}, "slow");
});

// ====== show modal window for codes select ======================================
// $('.show_modal').click(function() {
//     $('.codes_modal').html
//     modal.style.display = "block";
// });

// ====== expand text blocks onFocus ==============================================
$('.block_field__text').focus(function() {
   $(this).css("height", "36px").animate({height: '180px'}, "slow");
    // var obj = $('.block_field__text');
   // $(this).show().animate(null, {height: '180px'}, "slow")
   // animate();

});

// ====== collapse text block onBlur ==============================================
$('.block_field__text').blur(function() {
    $(this).css("height", "180px").show().animate({height: '36px'}, "slow");
});

// ====== save form data as new visit in DB =======================================
$('#submit_button').click(function () {
    $.datepicker.formatDate('dd.mm.yyyy', new Date());

    $.ajax({
        type: "POST",
        url: newVisitUrl,
        data: JSON.stringify({
            "causeOfTreatmentNote": $('#causeOfTreatmentNote').val(),
            "complaints": $('#complaints').val(),
            "diseaseAnamnesis": $('#diseaseAnamnesis').val(),
            "objectiveData": $('#objectiveData').val(),
            "doctorId": $('#selectVisitDoctor').children("option:selected").val(),
            "medCardId": 1,
            "fillDate": $('.colum_PIB_txtdt__date').val()
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            alert("success");
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    }); //ajax function
});