package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesType;
import com.beskydsoft.med.repo.AnalizesTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnalizesTypeService {
    @Autowired
    AnalizesTypeRepo analizesTypeRepo;

    public AnalizesType getAnalizesTypeById(Long id){
        return analizesTypeRepo.findById(id).get();
    }

}
