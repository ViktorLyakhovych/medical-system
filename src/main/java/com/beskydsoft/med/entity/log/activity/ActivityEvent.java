package com.beskydsoft.med.entity.log.activity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Inheritance(strategy= InheritanceType.JOINED)
@NoArgsConstructor
@AllArgsConstructor
public class ActivityEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String eventDescription;

    @OneToMany
    @JsonBackReference
    private Set<LogEvent> logEvents= new HashSet<>();
//            (
//            fetch = FetchType.EAGER,
//            cascade = CascadeType.ALL)
//    @JoinColumn(name = "logRecords_id")

}
