package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.LifeAnamnesis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LifeAnamnesisRepo extends CrudRepository<LifeAnamnesis, Long> {

}
