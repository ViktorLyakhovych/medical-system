package com.beskydsoft.med.entity.medcard.history;

import com.beskydsoft.med.entity.medcard.Episode;
import com.beskydsoft.med.entity.medcard.code.Code;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Diagnosis {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // кодування діагнозу
    @OneToOne(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "codes_id")
    private Code diagnosisCode;

    // примітка до діагнозу
    @Column(name = "diagnosisNote")
    private String diagnosisNote;

    // зворотній лінк на пацієнта (епізод)
    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "diagnosis")
    @JsonBackReference
    private Episode episode;


}
