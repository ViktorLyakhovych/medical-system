package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.data.gadget.UploadEvent;
import com.beskydsoft.med.entity.data.gadget.UploadedDataType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UploadEventRepo extends CrudRepository<UploadEvent, Long> {

    List<UploadEvent> findAllByPatientOrderByMeasureDateTimeDesc(Patient patient);

    List<UploadEvent> findAllByPatient(Patient patient);

    List<UploadEvent> findAllByDoctor(Doctor doctor);

    List<UploadEvent> findAllByPatientAndUploadedDataType(Patient patient, UploadedDataType uploadedDataType);
}
