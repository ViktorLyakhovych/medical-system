package com.beskydsoft.med.entity.log.activity;

import com.beskydsoft.med.entity.Patient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Inheritance(strategy= InheritanceType.JOINED)
@NoArgsConstructor
@AllArgsConstructor
public class LogEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm ")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "eventTime")
    private LocalDateTime eventTime;

    @ManyToOne
    @JsonIgnore
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "activity_event")
    private ActivityEvent activityEvent;




}
