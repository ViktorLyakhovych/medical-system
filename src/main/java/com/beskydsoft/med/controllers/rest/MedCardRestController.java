package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.medcard.MedCard;
import com.beskydsoft.med.service.MedCardService;
import com.beskydsoft.med.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedCardRestController {
    @Autowired
    PatientService patientService;
    @Autowired
    MedCardService medCardService;

    @GetMapping("/medCard/patient={patientId}")
    public MedCard getMedCardByPatientId(@PathVariable Long patientId){
        return patientService.getPatient(patientId).getMedCard();
    }

}
