package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.medcard.MedCard;
import com.beskydsoft.med.repo.MedCardRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MedCardService {
    @Autowired
    MedCardRepo medCardRepo;


    public MedCard getMedCardById(Long medCardId){
        Optional<MedCard> medCard = medCardRepo.findById(medCardId);
        return medCard.orElse(null);
    }

}
