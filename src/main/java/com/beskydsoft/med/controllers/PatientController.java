package com.beskydsoft.med.controllers;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.medcard.MedCard;
import com.beskydsoft.med.service.PatientService;
import com.beskydsoft.med.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
public class PatientController{
    private final PatientService patientService;
    private final UploadFileService uploadFileService;


    @Value("${upload.path.photos}")
    private String uploadPathPhotos;

    @Value("${upload.path}")
    private String uploadPath;

    @Autowired
    public PatientController(PatientService patientService,
                             UploadFileService uploadFileService) {
        this.patientService = patientService;
        this.uploadFileService = uploadFileService;
    }

    @GetMapping(value = "/patient/{id}")
    public String getPatient(@PathVariable Long id, Model model) {
        model.addAttribute(patientService.getPatient(id));
        return "PatientInfo";
    }

    // registration controller
    @PostMapping("/registration/patient/add")
    public String addUser(
            @RequestParam("file") MultipartFile file,
            @ModelAttribute Patient patient, Model model
    ) throws IOException {
        model.addAttribute("patient", patient);
        String fileName = uploadFileService.uploadFile(file, uploadPathPhotos);
        patient.setPhotoFileName(fileName);

        MedCard medCard = new MedCard();
        patient.setMedCard(medCard);

        patientService.addPatient(patient);
        return "patients";
    }

    @GetMapping(value = "/register/patient")
    public String patientReg(){
        return "registrationPatient";
    }
}
