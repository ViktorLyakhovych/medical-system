package com.beskydsoft.med.entity.data.gadget.uploads;

import com.beskydsoft.med.entity.data.gadget.UploadEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UploadFileEvent extends UploadEvent {

    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @Column(name = "uploaded_files_array")
//    @JoinColumn(name = "upload_event_id")
    @OrderColumn
    private UploadedFile[] uploadedFilesArray;

    public UploadedFile[] getUploadedFilesArray() {
        return uploadedFilesArray;
    }

}
