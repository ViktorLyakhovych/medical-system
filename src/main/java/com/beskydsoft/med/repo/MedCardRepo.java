package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.MedCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedCardRepo extends CrudRepository<MedCard, Long> {

}
