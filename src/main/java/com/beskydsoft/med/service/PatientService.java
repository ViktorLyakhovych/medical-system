package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.repo.DoctorRepo;
import com.beskydsoft.med.repo.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PatientService{
    @Autowired
    DoctorRepo doctorRepo;

    private final
    PatientRepo patientRepo;

    @Autowired
    public PatientService(PatientRepo patientRepo) {
        this.patientRepo = patientRepo;
    }

    /**
     * Gets all patients
     * @return instances of all patients
     */
    public Iterable<Patient> getAll(){
        return patientRepo.findAll();
    }

    /**
     * Gets one patient of specific doctor by patient id
     * @param patientId id of desired patient
     * @return instance of desired patient
     */
    public Patient getPatient(Long patientId) {

        return patientRepo.findById(patientId).get();
    }

    /**
     * saves the patient to DB
     * @param patient instance of desired patient
     */
    public void addPatient(Patient patient) {
        patientRepo.save(patient);
    }

    /**
     * check patient existence by its ID
     * @param patientId - ID of patient (type - Long)
     * @return boolean true or false
     */
    public boolean isPatientExists(Long patientId){
        return patientRepo.existsById(patientId);
    }

}
