package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.episode.BodyResearch;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BodyResearchRepo extends CrudRepository<BodyResearch, Long> {
}
