package com.beskydsoft.med.entity.medcard.history.appointment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // тип призначення
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "appointment_type")
    private AppointmentTypeLib appointmentType;

    // прийоми по призначенню
    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "appointment_id")
    private List<AppointmentTaking> appointmentTakings;

    //дата призначення
    @Column(name = "date")
    private LocalDate date;

    // назва
    @Column(name = "title")
    private String title;

    // опис
    @Column(name = "prescription")
    private String prescription;

    // доза
    @Column(name = "consumption_аmount")
    private String consumptionAmount;

    // кількість прийомів на день
    @Column(name = "consumption_сount")
    private int consumptionCount;

    // дні
    @Column(name = "consumption_days")
    private int consumptionDays;

    // тривалість прийому
    @Column(name = "duration")
    private int duration;

}
