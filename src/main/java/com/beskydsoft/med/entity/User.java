package com.beskydsoft.med.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Set;

@Getter
@Setter
@Entity
@Inheritance(strategy=InheritanceType
//        .SINGLE_TABLE)
//        .TABLE_PER_CLASS)
        .JOINED)
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    @Email
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "photo_file_name")
    private String photoFileName;

    @Column(name = "additional_info")
    private String additionalInfo;

    @ManyToMany
            (fetch = FetchType.EAGER)
    @JoinTable(name = "user_person_groups",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "person_group_id", referencedColumnName = "id"))
    private Set<PersonGroup> groups;

    public String getFullName(){
        return (getLastName() + ' ' + getFirstName() + ' ' + getSecondName());
    }

//    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
//    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
//    @Enumerated(EnumType.STRING)
//    private Set<Role> roles;






}
