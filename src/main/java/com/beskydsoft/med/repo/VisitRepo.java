package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.MedCard;
import com.beskydsoft.med.entity.medcard.Visit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VisitRepo extends CrudRepository<Visit, Long> {
    List<Visit> findAllByMedCardOrderByFillDate(MedCard medCard);
}
