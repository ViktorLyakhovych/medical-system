package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.data.gadget.UploadedDataType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadDataTypeRepo extends CrudRepository<UploadedDataType, Long> {
    UploadedDataType findDistinctFirstByUploadedDataTypeTitle(String up);
}
