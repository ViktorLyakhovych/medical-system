package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.history.appointment.Appointment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepo extends CrudRepository<Appointment, Long> {

//    @Query(value = "{call getAppointmentByPatientId(:pat)}", nativeQuery = true)
//    List<Appointment> findAllAppointmentsByPatientId(@Param("pat") Long pat);


}
