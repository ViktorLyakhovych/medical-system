package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.medcard.Visit;
import com.beskydsoft.med.entity.medcard.history.Action;
import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesAssignment;
import com.beskydsoft.med.repo.AnalizesAssignmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AnalizesAssignmentService {
    @Autowired
    AnalizesAssignmentRepo analizesAssignmentRepo;

    @Autowired
    PatientService patientService;

    public List<AnalizesAssignment> getAll() {
        return (List<AnalizesAssignment>) analizesAssignmentRepo.findAll();
    }

    public List<AnalizesAssignment> getAllByPatient(Long patientId) {
        List<AnalizesAssignment> analizesAssignments = new ArrayList<>();
        Patient patient = patientService.getPatient(patientId);
        Set<Action> actions = new HashSet<>();
        for (Visit visit: patient.getMedCard().getVisits()) {
            actions.addAll(visit.getActions());
        }
        for (Action action: actions) {
            analizesAssignments.addAll(action.getAnalizesAssignments());
        }
        return analizesAssignments;
    }

    public AnalizesAssignment findById(Long Id) {
        return analizesAssignmentRepo.findById(Id).get();
    }
}
