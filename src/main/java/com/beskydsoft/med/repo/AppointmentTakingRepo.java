package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.history.appointment.Appointment;
import com.beskydsoft.med.entity.medcard.history.appointment.AppointmentTaking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentTakingRepo extends CrudRepository<AppointmentTaking, Long> {
    int countByAppointment(Appointment appointment);
}
