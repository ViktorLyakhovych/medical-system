package com.beskydsoft.med.entity.data.web.uploads;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VisitJson {

    //медична картка
    private Long medCardId;

    //лікар
    private Long doctorId;

    //причини звернення (примітка)
    private String causeOfTreatmentNote;

    //скарги
    private String complaints;

    //анамнез хвороби
    private String diseaseAnamnesis;

    //об'єктивні дані
    private String objectiveData;

    //дата візиту
    private LocalDate dateOfVisit;
}
