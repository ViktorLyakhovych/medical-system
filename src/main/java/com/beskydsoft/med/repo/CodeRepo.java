package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.code.Code;
import com.beskydsoft.med.entity.medcard.code.CodeColor;
import com.beskydsoft.med.entity.medcard.code.CodeLetter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CodeRepo extends CrudRepository<Code, Long> {
    List<Code> findCodesByCodeColor(CodeColor codeColor);

    List<Code> findCodesByCodeLetter(CodeLetter codeLetter);

    Code findCodeByCodeLetterAndId(CodeLetter codeLetter, Long id);
}
