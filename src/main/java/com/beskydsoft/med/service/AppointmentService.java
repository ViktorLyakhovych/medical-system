package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.medcard.history.appointment.Appointment;
import com.beskydsoft.med.entity.medcard.history.appointment.AppointmentTaking;
import com.beskydsoft.med.repo.AppointmentRepo;
import com.beskydsoft.med.repo.AppointmentTakingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

@Service
public class AppointmentService {
    @Autowired
    AppointmentRepo appointmentRepo;
    @Autowired
    AppointmentTakingRepo appointmentTakingRepo;

    public Appointment getAppointment(Long id){
        return appointmentRepo.findById(id).get();
    }

    public int addAppointmentTaking(AppointmentTaking appointmentTaking){
        try {
            appointmentTakingRepo.save(appointmentTaking);
            return 200;
        } catch (Exception e){
            return 500;
        }
    }

    public int getAppointmentTakingsCount(Long appointmentId){
        return appointmentTakingRepo.countByAppointment(appointmentRepo.findById(appointmentId).get());
    }

    @ResponseStatus(HttpStatus.CREATED) // 201
    public void addAppointment(Appointment appointment){
        appointmentRepo.save(appointment);
    }
}
