package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.medcard.MedCard;
import com.beskydsoft.med.entity.medcard.Visit;
import com.beskydsoft.med.repo.VisitRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitService {
    @Autowired
    VisitRepo visitRepo;

    public List<Visit> getAllVisitsByPatient(Patient patient){
        MedCard medCard = patient.getMedCard();
        return visitRepo.findAllByMedCardOrderByFillDate(medCard);
    }

    public Visit getVisitById(Long visitId){
        return visitRepo.findById(visitId).get();
    }

    public void addVisit(Visit visit){
        visitRepo.save(visit);
    }

}
