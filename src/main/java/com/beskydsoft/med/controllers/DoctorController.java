package com.beskydsoft.med.controllers;

import com.beskydsoft.med.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/doctor/{id}")
    public String getDoctor(@PathVariable Long id, Model model) {
        model.addAttribute("doctor", doctorService.getDoctor(id));
        return "doctorInfo";
    }

    @GetMapping(value = "/consultants")
    public String getConsultants(){
        return "consultants";
    }



}
