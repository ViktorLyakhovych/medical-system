package com.beskydsoft.med.controllers;

import com.beskydsoft.med.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EnvironmentController {
    @Autowired
    private PatientService patientService;


    @GetMapping(value = "/settings")
    public String setEnvironment() {
        return "settings";
    }


    @GetMapping(value = "/")
    public String goToMainPage(Model model) {
        model.addAttribute("patients", patientService.getAll());
        return "patients";
    }


    @GetMapping(value = "/android/test/uploads")
    public String setUploads(){
        return "testingpost";
    }




}
