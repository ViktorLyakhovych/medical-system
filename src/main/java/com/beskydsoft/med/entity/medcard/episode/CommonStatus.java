package com.beskydsoft.med.entity.medcard.episode;

import com.beskydsoft.med.entity.medcard.Episode;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommonStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "commonStatus")
    @JsonBackReference
    private Episode episode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "common_status_position")
    private CommonStatusPositionLib commonStatusPosition;

    //дата заповнення
    @Column(name = "fill_date")
    private String fillDate;

    // свідомість
    @Column(name = "consciousness")
    private String consciousness;

    // стан
    @Column(name = "state")
    private String state;

    // вага
    @Column(name = "weight")
    private String weight;

    // зріст
    @Column(name = "height")
    private String height;

    // статура
    @Column(name = "stature")
    private String stature;

    // оцінка шкірних покривів
    @Column(name = "skin_assessment")
    private String skinAssessment;

    // оцінка слизових покривів
    @Column(name = "mucous_assessment")
    private String mucousAssessment;


}
