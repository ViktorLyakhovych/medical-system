package com.beskydsoft.med.controllers;

import com.beskydsoft.med.service.PatientService;
import org.aspectj.weaver.ast.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@Controller
public class TestController {
    @Autowired
    PatientService patientService;

    @GetMapping(value = "/post")
    public String getPostTestPage(){
        return "testingpost";
    }

    @GetMapping(value = "/info")
    public String getHomeDir(Model model) throws UnsupportedEncodingException {
        String currentUsersHomeDir = System.getProperty("user.home");
        model.addAttribute("path", currentUsersHomeDir);

        String currentUsersDir = System.getProperty("user.dir");
        model.addAttribute("pathDir", currentUsersDir);

        String path = Test.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPathTmp = URLDecoder.decode(path, "UTF-8");
        String decodedPath = decodedPathTmp + "";
        model.addAttribute("decodedPath", decodedPath);

        return "testPath";
    }
}
