package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.service.DoctorService;
import com.beskydsoft.med.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class DoctorRestController {
    @Autowired
    DoctorService doctorService;

    @Autowired
    PatientService patientService;

    @GetMapping("/android/patient/{patientId}/doctors/get")
    public Set<Doctor> getDoctorsByPatientId(
            @PathVariable Long patientId){
        return doctorService.getDoctorsByPatientId(patientId);
    }
}
