package com.beskydsoft.med.entity.medcard.code;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Code {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //код
    @Column(name = "codification")
    private String codification;

    //опис
    @Column(name = "description")
    private String description;

    //розділ (A-Z)
    @ManyToOne
    @JoinColumn(name = "code_letter_id")
    @JsonBackReference
    private CodeLetter codeLetter;


    //компонента (кодується кольором в таблиці)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "code_color_id")
    @JsonIgnore
    private CodeColor codeColor;
}
