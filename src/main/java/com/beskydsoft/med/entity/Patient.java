package com.beskydsoft.med.entity;

import com.beskydsoft.med.entity.data.gadget.UploadEvent;
import com.beskydsoft.med.entity.log.activity.LogEvent;
import com.beskydsoft.med.entity.medcard.MedCard;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Patient extends User {

    //дата народження
    @Column(name = "birthday")
    private LocalDate birthDate;

    // дата реєстрації
    @Column(name = "registrationDate")
    private LocalDate registrationDate;

    // адреса проживання
    @Column(name = "residence_address")
    private String residenceAddress;

    // резервний номер телефону
    @Column(name = "phone_number_reserve")
    private String phoneNumberReserve;

    // професія
    @Column(name = "profession")
    private String profession;

    // лікарі
    @ManyToMany
    @JoinTable(name = "patient_doctors",
            joinColumns = @JoinColumn(name = "patient_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "doctor_id", referencedColumnName = "id"))
    private Set<Doctor> doctors;

    // медична картка
    @OneToOne(
            cascade = CascadeType.ALL)
    @JoinColumn(name = "med_card_id")
    private MedCard medCard;

    // завантажені(відправлені лікарю) файли
    @OneToMany
            //(cascade = CascadeType.ALL) // не ставити. Подвійний запис в БД!
//    @JoinColumn(name = "upload_events_list_id")
    @JsonManagedReference
    private Set<UploadEvent> uploadEvents = new HashSet<>();


    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
//    @JoinColumn(name = "logEvents_id")
    private Set<LogEvent> logEvents = new HashSet<>();

    @Override
    public String toString() {
        return "Пацієнт{" +
                getLastName() + ' ' +
                getFirstName() + ' ' +
                getSecondName() +
                "}" +
                System.getProperty("line.separator");
    }

    public String getDiagnosis(){
        return "ДіагнозТест";
    }

//    public SimpleDateFormat getBirthDate() {
//        StringBuffer sb = new StringBuffer();
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
//        simpleDateFormat.format(birthDate, sb, new FieldPosition(0));
////        String patternUa = "dd.MM.yyyy";
////        DateFormatter formatter = ();
//        return simpleDateFormat;
//    }
}
