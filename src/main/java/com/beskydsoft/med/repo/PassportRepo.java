package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.Passport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassportRepo extends CrudRepository<Passport, Long> {

}
