package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesAssignment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnalizesAssignmentRepo extends CrudRepository<AnalizesAssignment, Long> {
}
