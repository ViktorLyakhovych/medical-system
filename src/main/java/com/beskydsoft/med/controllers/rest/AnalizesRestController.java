package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesAssignment;
import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesResult;
import com.beskydsoft.med.service.AnalizesAssignmentService;
import com.beskydsoft.med.service.AnalizesResultService;
import com.beskydsoft.med.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AnalizesRestController {
    @Autowired
    AnalizesAssignmentService analizesAssignmentService;

    @Autowired
    PatientService patientService;

    @Autowired
    AnalizesResultService analizesResultService;

    @GetMapping("/analizes/get/patient={Id}")
    public List<AnalizesAssignment> getAnalizesAssignments(@PathVariable Long Id) {
        return analizesAssignmentService.getAllByPatient(Id);
    }

    @GetMapping("/analizes/get/alldata/patient={patientId}")
    public List<AnalizesResult> getAnalizesResults(@PathVariable Long patientId){
//        Patient patient = patientService.getPatient(patientId);
        return analizesResultService.getAllAnalizesResultByPatient(patientId);
    }
}
