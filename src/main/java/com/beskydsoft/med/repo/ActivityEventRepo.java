package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.log.activity.ActivityEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityEventRepo extends CrudRepository<ActivityEvent, Long> {

    ActivityEvent findDistinctByEventDescription(String description);
}
