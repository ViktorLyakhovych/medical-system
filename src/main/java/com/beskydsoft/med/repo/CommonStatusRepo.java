package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.episode.CommonStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonStatusRepo extends CrudRepository<CommonStatus, Long> {
}
