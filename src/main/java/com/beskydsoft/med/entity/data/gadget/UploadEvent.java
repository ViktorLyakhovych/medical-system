package com.beskydsoft.med.entity.data.gadget;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.entity.Patient;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Inheritance(strategy=InheritanceType.
        //JOINED)
        SINGLE_TABLE)
@NoArgsConstructor
@AllArgsConstructor
public class UploadEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonBackReference
    private Patient patient;

    @ManyToOne
    @JsonBackReference
    private Doctor doctor;

    public String getDoctorName(){
        return doctor.getFullName();
    }

    // дата вимірювання. Фіксується на стороні пацієнта і передається на сервер
    @Column(name = "measure_date")
    private String measureDateTime;
//    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
//    private LocalDateTime measureDateTime;

    // дата завантаження(отримання сервером). Фіксується на сервері в момент отримання даних
    @Column(name = "upload_date")
    private String uploadDateTime;
//    private LocalDateTime uploadDateTime;

    // тип завантажених даних
    //  Прості - пульс, температура, кров'яний тиск.
    //  Складні(з файлами) - UploadFileEvent(ECG, ECHOGRAM etc.)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "uploaded_data_type")
    private UploadedDataType uploadedDataType;

    // примітка
    @Column(name = "description")
    private String description;

    // повертає українську назву
    @JsonIgnore
    public String getDataType(){
        return uploadedDataType.getUploadedDataTypeExplanation();
    }

    // повертає умовне позначення-код
    @JsonIgnore
    public String getDataTypeTitle(){
        return uploadedDataType.getUploadedDataTypeTitle();
    }
}
