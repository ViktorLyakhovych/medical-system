package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.medcard.code.Code;
import com.beskydsoft.med.entity.medcard.code.CodeColor;
import com.beskydsoft.med.entity.medcard.code.CodeLetter;
import com.beskydsoft.med.repo.CodeColorRepo;
import com.beskydsoft.med.repo.CodeLetterRepo;
import com.beskydsoft.med.repo.CodeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CodeService {
    @Autowired
    CodeRepo codeRepo;

    @Autowired
    CodeColorRepo codeColorRepo;

    @Autowired
    CodeLetterRepo codeLetterRepo;

    public List<Code> getCodesByColor(Long codeColorId){
        CodeColor codeColor = codeColorRepo.findById(codeColorId).get();
        return codeRepo.findCodesByCodeColor(codeColor);
    }

    public List<CodeLetter> getAllCodeLetters(){
        return (List<CodeLetter>)codeLetterRepo.findAll();
    }

    public List<CodeColor> getAllCodeColors(){
        return (List<CodeColor>) codeColorRepo.findAll();
    }

    public List<Code> getCodesByLetter(char letter){
        return codeRepo.findCodesByCodeLetter(getCodeLetterByLetter(letter));
    }

    public Code getCodeByLetterAndId(char letter, Long id){
        return codeRepo.findCodeByCodeLetterAndId(getCodeLetterByLetter(letter), id);
    }

    public CodeLetter getCodeLetterByLetter(char letter){
        return codeLetterRepo.findAllByLetter(letter);
    }

    public List<Character> getAllChars(){
        List<Character> chars = new ArrayList<Character>();
        for (CodeLetter cl: getAllCodeLetters()) {
            chars.add(cl.getLetter());
        }
        return  chars;
    }
}
