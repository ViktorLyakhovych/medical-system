package com.beskydsoft.med.config;

import com.beskydsoft.med.service.DoctorService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class MonimedAuthenticationProvider implements AuthenticationProvider {

    private DoctorService dotorService;

    public MonimedAuthenticationProvider(DoctorService dotorService) {
        this.dotorService = dotorService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String name = authentication.getName();
        final String password = authentication.getCredentials().toString();
        verifyLdapAuthentication(name, password);
        final List<GrantedAuthority> grantedAuths = new ArrayList<>();
        grantedAuths.add(new SimpleGrantedAuthority("USER"));
        final UserDetails principal = new User(name, password, grantedAuths);
        return new UsernamePasswordAuthenticationToken(principal, password, grantedAuths);
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private void verifyLdapAuthentication(String name, String password) {
        if (dotorService.findByCredentials(name, password) == null) {
            throw new AuthenticationException("") {
            };

        }
    }

}
