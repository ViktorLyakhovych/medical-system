package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.episode.CommonStatusPositionLib;
import org.springframework.data.repository.CrudRepository;

public interface CommonStatusPositionLibRepo extends CrudRepository<CommonStatusPositionLib, Long> {
}
