create table monimed.activity_event
(
	id bigint auto_increment
		primary key,
	description varchar(255) null
);

create table monimed.analizes_type
(
    id bigint auto_increment
        primary key,
    title varchar(255) null,
    type_explanation varchar(255) null
);

create table monimed.analizes_result
(
	id bigint auto_increment
		primary key,
	data_file_name varchar(255) null,
	date date null,
	analizes_type_id bigint null,
	constraint FK4ef6tu8v0vs8nvmqa207lludw
		foreign key (analizes_type_id) references monimed.analizes_type (id)
);

create table monimed.appointment_type_lib
(
    id bigint auto_increment
        primary key,
    appointment_type varchar(255) null,
    appointment_type_explanation varchar(255) null
);

create table monimed.code_color
(
	id bigint auto_increment
		primary key,
	hex_code varchar(255) null,
	title varchar(255) null
);

create table monimed.code_letter
(
	id bigint auto_increment
		primary key,
	letter char null,
	title varchar(255) null
);

create table monimed.code
(
	id bigint auto_increment
		primary key,
	codification varchar(255) null,
	description varchar(255) null,
	code_color_id bigint null,
	code_letter_id bigint null,
	constraint FKaej1brq4j83t91t9v25fpclko
		foreign key (code_color_id) references monimed.code_color (id),
	constraint FKamd9t8svaiujdlk8evrodunsq
		foreign key (code_letter_id) references monimed.code_letter (id)
);

create table monimed.code_color_codes
(
	code_color_id bigint not null,
	codes_id bigint not null,
	constraint UK_t6xenc60n4o2ysryr9hpcekdk
		unique (codes_id),
	constraint FKn4nip94xxc1tg210hfjwjdy9n
		foreign key (code_color_id) references monimed.code_color (id),
	constraint FKsj14t2495fyu67qmawcwk8inr
		foreign key (codes_id) references monimed.code (id)
);

create table monimed.code_letter_codes
(
	code_letter_id bigint not null,
	codes_id bigint not null,
	constraint UK_nypyejerx1poh0bmknn4e5syw
		unique (codes_id),
	constraint FK4fp1gudsvw370dsr5bkaemppv
		foreign key (code_letter_id) references monimed.code_letter (id),
	constraint FK7ivn6460be3xl6cpkkrm2odg3
		foreign key (codes_id) references monimed.code (id)
);

create table monimed.common_status_position_lib
(
	id bigint auto_increment
		primary key,
	common_status_position varchar(255) null
);

create table monimed.diagnosis
(
	id bigint auto_increment
		primary key,
	diagnosis_note varchar(255) null,
	codes_id bigint null,
	constraint FKcnntdw6hn51hvgbbij8j32hk1
		foreign key (codes_id) references monimed.code (id)
);

create table monimed.life_anamnesis
(
	id bigint auto_increment
		primary key,
	allergic_analysis varchar(255) null,
	bad_habbits varchar(255) null,
	childhood_progress_specifics varchar(255) null,
	epidemiological_analysis varchar(255) null,
	fill_date date null,
	gynecological_analysis varchar(255) null,
	place_of_birth varchar(255) null,
	transmitted_diseases varchar(255) null
);

create table monimed.passport
(
	id bigint auto_increment
		primary key,
	accommodation varchar(255) null,
	education varchar(255) null,
	family_members_sick varchar(255) null,
	fill_date date null,
	injuries varchar(255) null,
	invalidity varchar(255) null,
	marital_status varchar(255) null,
	operations varchar(255) null,
	prison varchar(255) null,
	sex varchar(255) null
);

create table monimed.med_card
(
	id bigint auto_increment
		primary key,
	life_anamnesis_id bigint null,
	passport_id bigint null,
	constraint FKgex7fam0emmjflbn2rlo983uf
		foreign key (passport_id) references monimed.passport (id),
	constraint FKks12ur83u7e85njsr5uuuq83b
		foreign key (life_anamnesis_id) references monimed.life_anamnesis (id)
);

create table monimed.common_status
(
	id bigint auto_increment
		primary key,
	consciousness varchar(255) null,
	fill_date varchar(255) null,
	height varchar(255) null,
	mucous_assessment varchar(255) null,
	skin_assessment varchar(255) null,
	state varchar(255) null,
	stature varchar(255) null,
	weight varchar(255) null,
	common_status_position bigint null,
	constraint FK75f403cfdn1drxolxs42wbkku
		foreign key (common_status_position) references monimed.common_status_position_lib (id)
);

create table monimed.episode
(
	id bigint auto_increment
		primary key,
	common_status_id bigint null,
	diagnosis_id bigint null,
	med_card_id bigint null,
	constraint FKhh98s4psr7nh7b7t7i7i7nkhh
		foreign key (med_card_id) references monimed.med_card (id),
	constraint FKo9w3f2eo002g9uxyv5lnpuwec
		foreign key (common_status_id) references monimed.common_status (id),
	constraint FKov9bu6h51h7lt4q97fpf2nxo1
		foreign key (diagnosis_id) references monimed.diagnosis (id)
);

create table monimed.body_research
(
	id bigint auto_increment
		primary key,
	cardiovascular_system varchar(255) null,
	digestive_system varchar(255) null,
	endocrine_system varchar(255) null,
	fill_date varchar(255) null,
	genitourinary_system varchar(255) null,
	peripheral_edema bit null,
	respiratory_system varchar(255) null,
	episode_id bigint null,
	constraint FKc5hawp1lgbvdwc93h6t5pvivy
		foreign key (episode_id) references monimed.episode (id)
);

create table monimed.person_group
(
	id bigint auto_increment
		primary key,
	title varchar(255) null
);

create table monimed.user
(
	id bigint auto_increment
		primary key,
	first_name varchar(255) null,
	second_name varchar(255) null,
	last_name varchar(255) null,
	phone_number varchar(255) null,
	email varchar(255) null,
	additional_info varchar(255) null,
	photo_file_name varchar(255) null
);

create table monimed.doctor
(
	category varchar(255) null,
	license_number varchar(255) null,
	license_start_date date null,
	password varchar(255) null,
	specialization varchar(255) null,
	tax_number varchar(255) null,
	work_experience_start date null,
	id bigint not null
		primary key,
	constraint FKisrj7dti092bxya7p8jt7acs7
		foreign key (id) references monimed.user (id)
);

create table monimed.patient
(
	birthday date null,
	phone_number_reserve varchar(255) null,
	profession varchar(255) null,
	registration_date date null,
	residence_address varchar(255) null,
	id bigint not null
		primary key,
	med_card_id bigint null,
	constraint FKbhxnsr0osyqj98qqcexec5edv
		foreign key (id) references monimed.user (id),
	constraint FKq8nfdx8nysj6rnwag1h73pqxp
		foreign key (med_card_id) references monimed.med_card (id)
);

create table monimed.log_event
(
	id bigint auto_increment
		primary key,
	event_time datetime null,
	activity_event bigint null,
	patient_id bigint null,
	constraint FKl16q0nxxbctjkhfnamcdkhhtx
		foreign key (activity_event) references monimed.activity_event (id),
	constraint FKo7q65ekkjdbllmmtmyg0mpa5
		foreign key (patient_id) references monimed.patient (id)
);

create table monimed.activity_event_log_events
(
	activity_event_id bigint not null,
	log_events_id bigint not null,
	primary key (activity_event_id, log_events_id),
	constraint UK_3t0ecomqxebfr7uga56seuj8d
		unique (log_events_id),
	constraint FK61cbylgp6ovvb8nviabumrmh6
		foreign key (log_events_id) references monimed.log_event (id),
	constraint FKk9e8ff7pah3qwn027f5w11np7
		foreign key (activity_event_id) references monimed.activity_event (id)
);

create table monimed.patient_doctors
(
	patient_id bigint not null,
	doctor_id bigint not null,
	primary key (patient_id, doctor_id),
	constraint FKnd7qycxy9alqdms54ux6nx7h8
		foreign key (patient_id) references monimed.patient (id),
	constraint FKti6nth5gnindrxstb1lgc8viw
		foreign key (doctor_id) references monimed.doctor (id)
);

create table monimed.patient_log_events
(
	patient_id bigint not null,
	log_events_id bigint not null,
	primary key (patient_id, log_events_id),
	constraint UK_64nlyr968syfgvkiaef6aka6i
		unique (log_events_id),
	constraint FKejara8q1p04bq12iu5yaq94xx
		foreign key (log_events_id) references monimed.log_event (id),
	constraint FKs8l2owrfhpltqvuprm4ggl0uo
		foreign key (patient_id) references monimed.patient (id)
);


create table monimed.user_person_groups
(
	user_id bigint not null,
	person_group_id bigint not null,
	primary key (user_id, person_group_id),
	constraint FK4nsmd10onj59c9lbyer17oo40
		foreign key (user_id) references monimed.user (id),
	constraint FKi2wkl3n7skbjj20u4oduotutc
		foreign key (person_group_id) references monimed.person_group (id)
);

create table monimed.visit
(
	id bigint auto_increment
		primary key,
	cause_of_treatment_note varchar(255) null,
	complaints varchar(255) null,
	disease_anamnesis varchar(255) null,
	fill_date date null,
	objective_data varchar(255) null,
	doctor_id bigint null,
	med_card_id bigint null,
	constraint FK8uqn39026fg7pll0q6limce0s
		foreign key (med_card_id) references monimed.med_card (id),
	constraint FKc63541y8ppkvsovm00gumv90t
		foreign key (doctor_id) references monimed.doctor (id)
);

create table monimed.action
(
	id bigint auto_increment
		primary key,
	visit_id bigint null,
	constraint FKbc69wg6tpsbohbwx3io5aj4wk
		foreign key (visit_id) references monimed.visit (id)
);

create table monimed.action_action_codes
(
	action_id bigint not null,
	action_codes_id bigint not null,
	primary key (action_id, action_codes_id),
	constraint UK_egsgbi587hcs8crt2p32rcvle
		unique (action_codes_id),
	constraint FKa6uc28gca1rm6tanrp199fttv
		foreign key (action_id) references monimed.action (id),
	constraint FKkbe66vrb2oprx3wfxo7nhu7vd
		foreign key (action_codes_id) references monimed.code (id)
);

create table monimed.analizes_assignment
(
	id bigint auto_increment
		primary key,
	fill_date date null,
	action_id bigint null,
	analizes_result_id bigint null,
	analizes_type_id bigint null,
	constraint FK1tv8l42tdlwinwsql3pycua08
		foreign key (analizes_result_id) references monimed.analizes_result (id),
	constraint FK9sl12id6nau6dkwkb1abbrxok
		foreign key (action_id) references monimed.action (id),
	constraint FKpb58ca1e7e04wjj2qh5ont9wm
		foreign key (analizes_type_id) references monimed.analizes_type (id)
);

create table monimed.appointment
(
	id bigint auto_increment
		primary key,
	consumption_аmount varchar(255) null,
	consumption_сount int null,
	consumption_days int null,
	date date null,
	duration int null,
	prescription varchar(255) null,
	title varchar(255) null,
	appointment_type bigint null,
	action_id bigint null,
	constraint FKjoofgtg8lwyxoe62axfmwf52x
		foreign key (action_id) references monimed.action (id),
	constraint FKkhypew17ky7esp79gxpooy4a7
		foreign key (appointment_type) references monimed.appointment_type_lib (id)
);

create table monimed.episode_visits
(
	episode_id bigint not null,
	visit_id bigint not null,
	primary key (episode_id, visit_id),
	constraint FKjofsc145j7sww6oxpcxpoax1d
		foreign key (episode_id) references monimed.episode (id),
	constraint FKnxvelyro0josj7mxgqgetpjk4
		foreign key (visit_id) references monimed.visit (id)
);

create table monimed.visit_codes
(
    visit_id bigint not null,
    code_id bigint not null,
    primary key (visit_id, code_id),
    constraint UK_tpah4ut30e09hq233mdnastf4
        unique (code_id),
    constraint FK6grpiuqqt0wg7wvdrwc2qsa5h
        foreign key (code_id) references monimed.code (id),
    constraint FK80aslnvgeq6cdea9dd3ckl179
        foreign key (visit_id) references monimed.visit (id)
);

