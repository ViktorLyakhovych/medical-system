package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.log.activity.LogEvent;
import com.beskydsoft.med.repo.ActivityEventRepo;
import com.beskydsoft.med.repo.LogEventRepo;
import com.beskydsoft.med.repo.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class LogEventService {
    @Autowired
    LogEventRepo logEventRepo;
    @Autowired
    PatientRepo patientRepo;
    @Autowired
    ActivityEventRepo activityEventRepo;

    public LogEvent createNewLogEvent(Long eventId, Long patientId){
        LogEvent logEvent = new LogEvent();
        logEvent.setPatient(patientRepo.findById(patientId).get());
        logEvent.setActivityEvent(activityEventRepo.findById(eventId).get());
        logEvent.setEventTime(LocalDateTime.now());
        return logEvent;
    }

    public void saveLogEvent(LogEvent logEvent){
        logEventRepo.save(logEvent);
    }

//
//    public void addLogEvent(LogEvent logEvent){
//        logEventRepo.save(logEvent);
//    }

    public Iterable<LogEvent> getAllLogEvents(){
        return logEventRepo.findAll();
    }

    public Iterable<LogEvent> getAllLogEventsByPatient(Long patientId) {
        Patient patient = patientRepo.findById(patientId).get();
        return logEventRepo.findAllByPatient(patient);
    }


}
