package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.data.gadget.UploadEvent;
import com.beskydsoft.med.entity.data.gadget.UploadedDataType;
import com.beskydsoft.med.service.DoctorService;
import com.beskydsoft.med.service.PatientService;
import com.beskydsoft.med.service.UploadEventService;
import com.beskydsoft.med.service.UploadedDataTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UploadRestController {
    @Autowired
    UploadEventService uploadEventService;
    @Autowired
    PatientService patientService;
    @Autowired
    DoctorService doctorService;
    @Autowired
    UploadedDataTypeService uploadedDataTypeService;


    // всі дані від пацієнта
    @GetMapping(value = "/android/get/uploads/patient/{id}")
    public List<UploadEvent> getUploadDataByPatientId(@PathVariable Long id){
        return uploadEventService.getUploadEventsByPatient(patientService.getPatient(id));
    }

    // всі дані від пацієнта сортовані по даті надсилання (пацієнтом)
    @GetMapping(value = "/get/uploads/patient/{id}/ordered")
    public List<UploadEvent> getUploadDataByPatientIdOrdered(@PathVariable Long id){
        return uploadEventService.getUploadEventsByPatientOrdered(patientService.getPatient(id));
    }

    // вилучити дані від пацієнта по їх ІД (після перегляду, якщо неважливі)
    @GetMapping(value = "/del/uploads/{id}")
    public void delUploadEventById(@PathVariable Long id){
        uploadEventService.delUploadEventById(id);
    }

    // всі дані для одного лікаря по його ІД
    @GetMapping(value = "/get/uploads/doctor/{id}")
    public List<UploadEvent> getAllUploadEventsByDoctorId(@PathVariable Long id){
        return uploadEventService.getUploadEventsByDoctorOrdered(doctorService.getDoctor(id));
    }

    //всі дані пульсу від пацієнта для тесту (треба буде в фільтруванні даних на вюшці, тому не стираю)
    @GetMapping(value = "/android/pulses/patient/{id}")
    public List<UploadEvent> getPulsesByPatient(@PathVariable Long id){
        UploadedDataType uploadedDataType = uploadedDataTypeService.getUploadDataTypeByFileType("PULSE");
        return uploadEventService.getUploadEventsByPatientAndType(patientService.getPatient(id), uploadedDataType);
    }
}
