package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesResult;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnalizesResultRepo extends CrudRepository<AnalizesResult, Long> {
}
