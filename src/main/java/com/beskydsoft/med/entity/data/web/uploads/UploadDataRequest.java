package com.beskydsoft.med.entity.data.web.uploads;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UploadDataRequest {

    private Long doctorId;
//    private List<Long> episodes;

}
