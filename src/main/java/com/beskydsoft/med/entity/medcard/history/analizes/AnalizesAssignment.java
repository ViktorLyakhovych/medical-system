package com.beskydsoft.med.entity.medcard.history.analizes;

import com.beskydsoft.med.entity.medcard.history.Action;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
//@Inheritance(strategy= InheritanceType.JOINED)
@NoArgsConstructor
@AllArgsConstructor
public class AnalizesAssignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //дата заповнення (звернення)
    @Column(name = "fillDate")
    private LocalDate fillDate;

    // зворотній зв'язок на дію після візиту, внаслідок якої призначаються аналізи
    @ManyToOne
            (fetch = FetchType.LAZY)
    @JsonBackReference
    private Action action;

    // тип призначених аналізів
    @OneToOne(
            fetch = FetchType.EAGER
            )
    @JoinColumn(name = "analizesType_id")
    private AnalizesType analizesType;

    // результат аналізів
    @OneToOne(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "analizes_result_id")
    private AnalizesResult analizesResult;





}
