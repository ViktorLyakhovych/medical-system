     function searchAction() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("input_query");
        filter = input.value.toUpperCase();
        ul = document.getElementById("ul_section");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
          a = li[i].getElementsByTagName("a")[0];
          txtValue = a.textContent || a.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
          } else {
            li[i].style.display = "none";
          }
        }
      }

      var coll = document.getElementsByClassName("collapsible");
      var i;

      for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
          this.classList.toggle("active");

          patients = document.getElementById("patients_online_block");

          var content = this.nextElementSibling;
          if (content.style.display === "block") {
            content.style.display = "none";
            patients.style.height = "55px";
          } else {
            content.style.display = "block";
            patients.style.height = "460px";
          }
        });
      }