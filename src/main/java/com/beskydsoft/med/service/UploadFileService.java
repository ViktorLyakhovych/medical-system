package com.beskydsoft.med.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class UploadFileService {

    private static final Logger log = Logger.getLogger(UploadFileService.class);

    public String getUUIDFileName(String oldFileName){
        return UUID.randomUUID().toString() + "." + oldFileName;
    }

    public void makeFolder(String uploadPath){
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
    }

    public String uploadFile(MultipartFile file, String pathToUpload) throws IOException {
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            makeFolder(pathToUpload);
            String resultFilename = getUUIDFileName(file.getOriginalFilename());
            file.transferTo(new File(pathToUpload + "/" + resultFilename));
            return resultFilename;
        }
        log.error("file is not chosen or didn't exist");
        return null;
    }
}
