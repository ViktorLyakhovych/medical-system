package com.beskydsoft.med.entity.medcard.episode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommonStatusPositionLib {

//    ACTIVE("Активна"),
//    PASSIVE("Пасивна"),
//    FORCED("Вимушена");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "common_status_position")
    private String commonStatusPosition;

}
