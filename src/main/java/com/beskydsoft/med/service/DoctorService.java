package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.repo.DoctorRepo;
import com.beskydsoft.med.repo.PatientRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class DoctorService {

    private final DoctorRepo doctorRepo;
    private final PatientRepo patientRepo;
    private static List<Doctor> doctors;

    private static final Logger log = Logger.getLogger(DoctorService.class);

    static {
        doctors = new ArrayList<>();
        // doctors
        Doctor doc1 = new Doctor();
        doc1.setFirstName("Петро");
        doc1.setLastName("Никодименко");
        doc1.setSecondName("Іванович");
        doc1.setEmail("t@t");
        doc1.setPassword("t");
        doc1.setPhoneNumber("380971234567");
        doc1.setAdditionalInfo("Кардіолог");
        doc1.setTaxNumber("1112223334");
        doc1.setId(1L);
        doctors.add(doc1);
    }

    @Autowired
    public DoctorService(DoctorRepo doctorRepo, PatientRepo patientRepo) {
        this.doctorRepo = doctorRepo;
        this.patientRepo = patientRepo;
    }

    public Doctor findByCredentials(String mail, String pass) {
        return doctors
                .stream()
                .filter(d -> d.getEmail().equals(mail) && d.getPassword().equals(pass))
                .findAny()
                .orElse(null);
    }

    public Doctor getDoctor(Long id) {
        Optional<Doctor> doctor = doctorRepo.findById(id);
        return doctor.orElse(null);
    }
    
    public void addDoctor(Doctor doctor){
        doctorRepo.save(doctor);
    }

    public Set<Doctor> getDoctorsByPatientId(long patientId){
        Patient patient = patientRepo.findById(patientId).get();
        return patient.getDoctors();
    }
}
