$("#login_form").submit(function (event) {
    event.preventDefault();
    var email = $('#email').val();
    var pass = $('#password').val();
    if (email === "" || pass === "") {
        if (email === "") {
            $.notify("Заповніть поле: Електронна пошта", "error");
            $('#email').css("border","1px solid #de5114");
        }
        if (pass === "") {
            $.notify("Заповніть поле: Пароль", "error");
            $('#password').css("border","1px solid #de5114");
        }
    } else {
        $.ajax({
            url: '/login',
            dataType: 'json',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({"email": email, "password": pass}),
            processData: false,
            success: function (data, textStatus, jQxhr) {
                $(location).attr('href', "/patients");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $.notify("Не правильний логін/пароль");
            }
        });
    }


});