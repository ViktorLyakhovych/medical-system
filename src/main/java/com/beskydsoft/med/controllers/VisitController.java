package com.beskydsoft.med.controllers;

import com.beskydsoft.med.controllers.rest.AppointmentRestController;
import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.data.web.uploads.VisitJson;
import com.beskydsoft.med.entity.medcard.Visit;
import com.beskydsoft.med.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class VisitController {
     @Autowired
    VisitService visitService;

    @Autowired
    PatientService patientService;

    @Autowired
    UploadEventService uploadEventService;

    @Autowired
    AppointmentRestController appointmentRestController;

    @Autowired
    DoctorService doctorService;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    AnalizesAssignmentService analizesAssignmentService;

    @GetMapping("/patient/visits/get/{patientId}")
    public String getAllVisitsByPatientId(
            @PathVariable Long patientId,
            Model model
    ){
        Patient patient = patientService.getPatient(patientId); //separate variable because of two refers further
        model.addAttribute("patients", patientService.getAll());
        model.addAttribute("patient", patient);
        model.addAttribute("doctors", doctorService.getDoctorsByPatientId(patientId));
        model.addAttribute("visits", visitService.getAllVisitsByPatient(patient));
        model.addAttribute("episodes", episodeService.getAllEpisodesByPatient(patient));
        model.addAttribute("analizes", analizesAssignmentService.getAllByPatient(patientId));
        model.addAttribute("uploadEvents", uploadEventService.getUploadEventsByPatientOrdered(patient));
        model.addAttribute("appointments", appointmentRestController.getAppointmentsGroupByPatientId(patientId));
        return "visits";
    }

    @GetMapping("/patient/{patientId}/visit/{visitId}")
    public String getVisit(
            @PathVariable Long visitId,
            @PathVariable Long patientId,
            Model model){
        model.addAttribute("visit", visitService.getVisitById(visitId));
        model.addAttribute("patient", patientService.getPatient(patientId));
        model.addAttribute("patients", patientService.getAll());
        model.addAttribute("doctors", doctorService.getDoctorsByPatientId(patientId));
        return "visit";
    }



}
