package com.beskydsoft.med.entity.medcard;

import com.beskydsoft.med.entity.Patient;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Inheritance(strategy=InheritanceType.JOINED)
@NoArgsConstructor
@AllArgsConstructor
public class MedCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // зворотній лінк на пацієнта
    @OneToOne(fetch = FetchType.LAZY,
            mappedBy = "medCard")
    @JsonBackReference
    private Patient patient;

    // паспорт
    @OneToOne(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "passport_id")
    private Passport passport;

    // анамнез життя
    @OneToOne(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "life_anamnesis_id")
    private LifeAnamnesis lifeAnamnesis;

    // епізоди (захворювання-діагнози)
    @OneToMany
//            (fetch = FetchType.EAGER,
//            cascade = CascadeType.ALL)
    @JoinColumn(name = "med_card_id")
    private Set<Episode> episodes = new HashSet<>();

    // записи в логах активності
    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "med_card_id")
    private Set<Visit> visits = new HashSet<>();

}
