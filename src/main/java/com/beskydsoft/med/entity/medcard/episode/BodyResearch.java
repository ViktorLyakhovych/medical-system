package com.beskydsoft.med.entity.medcard.episode;

import com.beskydsoft.med.entity.medcard.Episode;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BodyResearch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonBackReference
    private Episode episode;

    //дата заповнення
    @Column(name = "fill_date")
    private String fillDate;

    // серцево-судинна система
    @Column(name = "cardiovascular_system")
    private String cardiovascularSystem;

    // дихальна система
    @Column(name = "respiratory_system")
    private String respiratorySystem;

    // травна система
    @Column(name = "digestive_system")
    private String digestiveSystem;

    // Сечостатева система
    @Column(name = "genitourinary_system")
    private String genitourinarySystem;

    // Ендокринна система
    @Column(name = "endocrine_system")
    private String endocrineSystem;

    // Периферичні набряки
    @Column(name = "peripheral_edema")
    private boolean peripheralEdema;


}
