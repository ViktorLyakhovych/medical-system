package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.data.gadget.UploadEvent;
import com.beskydsoft.med.entity.data.gadget.UploadedDataType;
import com.beskydsoft.med.repo.PatientRepo;
import com.beskydsoft.med.repo.UploadEventRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UploadEventService {
    @Autowired
    UploadEventRepo uploadEventRepo;
    @Autowired
    PatientRepo patientRepo;

    public void addUploadEvent(UploadEvent uploadEvent) {
        uploadEventRepo.save(uploadEvent);
    }

    public List<UploadEvent> getUploadEventsByPatient(Patient patient){
        return uploadEventRepo.findAllByPatient(patient);
    }

    public List<UploadEvent> getUploadEventsByPatientOrdered(Patient patient){
        return uploadEventRepo.findAllByPatientOrderByMeasureDateTimeDesc(patient);
    }

    public List<UploadEvent> getUploadEventsByPatientAndType(Patient patient, UploadedDataType uploadedDataType){
        return uploadEventRepo.findAllByPatientAndUploadedDataType(patient, uploadedDataType);
    }

    public List<UploadEvent> getUploadEventsByDoctorOrdered(Doctor doc){
        return uploadEventRepo.findAllByDoctor(doc);
    }

    public void delUploadEventById(Long id){
        uploadEventRepo.deleteById(id);
    }
}
