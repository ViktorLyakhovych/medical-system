package com.beskydsoft.med.entity.medcard.code;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CodeColor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //назва компоненти
    @Column(name = "title")
    private String title;

    //назва компоненти
    @Column(name = "hex_code")
    private String hex_code;

    //зв. зв'язок з кодами
    @OneToMany
    @JsonBackReference
    private List<Code> codes;
}
