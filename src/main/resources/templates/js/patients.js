$(document).ready(function () {
    $("#switch_to_cart_id").click(function () {
        $.ajax({
            type: "POST",
            url: "/patients/switch/cart",
            success: function () {
                location.reload();
            }
        });
    });

    $("#switch_to_list_id").click(function () {
        $.ajax({
            type: "POST",
            url: "/patients/switch/list",
            success: function () {
                location.reload();
            }
        });
    });
});
