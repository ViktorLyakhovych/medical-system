package com.beskydsoft.med.entity.medcard.history;

import com.beskydsoft.med.entity.medcard.Visit;
import com.beskydsoft.med.entity.medcard.code.Code;
import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesAssignment;
import com.beskydsoft.med.entity.medcard.history.appointment.Appointment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Action {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //коди призначених дій
    @OneToMany(
            fetch = FetchType.EAGER)
    private Set<Code> actionCodes = new HashSet<>();

    // призначення аналізів
    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "action_id")
    private Set<AnalizesAssignment> analizesAssignments = new HashSet<>();

    // призначення лікування
    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "action_id")
    private Set<Appointment> appointment = new HashSet<>();

    // зворотній зв'язок на візит
    @ManyToOne
            (fetch = FetchType.LAZY)
    @JsonBackReference
    private Visit visit;
}
