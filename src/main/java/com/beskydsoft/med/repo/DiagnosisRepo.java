package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.history.Diagnosis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiagnosisRepo extends CrudRepository<Diagnosis, Long> {
}
