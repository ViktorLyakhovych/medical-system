package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.medcard.Episode;
import com.beskydsoft.med.service.EpisodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class EpisodeRestController {
    @Autowired
    EpisodeService episodeService;

    @GetMapping("/patient/{patientId}/episodes")
    public Set<Episode> getAllEpisodesByPatientId(@PathVariable Long patientId){
        return (Set<Episode>)episodeService.getAllEpisodesByPatientId(patientId);
    }

    @GetMapping("/patient/{patientId}/episode={episodeId}")
    public Episode getOneEpisodeByPatientId(@PathVariable Long patientId, @PathVariable Long episodeId){
        return episodeService.getEpisodeByPatientId(patientId, episodeId);
    }
}
