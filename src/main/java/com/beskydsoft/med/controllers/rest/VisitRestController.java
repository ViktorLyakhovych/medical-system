package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.data.web.uploads.VisitJson;
import com.beskydsoft.med.entity.medcard.MedCard;
import com.beskydsoft.med.entity.medcard.Visit;
import com.beskydsoft.med.service.DoctorService;
import com.beskydsoft.med.service.MedCardService;
import com.beskydsoft.med.service.PatientService;
import com.beskydsoft.med.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
public class VisitRestController {
    @Autowired
    PatientService patientService;

    @Autowired
    VisitService visitService;

    @Autowired
    DoctorService doctorService;

    @Autowired
    MedCardService medCardService;

    // returns all patient's visits by patient's id
    @GetMapping("/visits/patient={patientId}")
    public Set<Visit> getallVisitsByPatientId(@PathVariable Long patientId){
        return patientService.getPatient(patientId).getMedCard().getVisits();
    }

    @GetMapping("/rest/visit/{visitId}")
    public Visit getVisitBuPatientIdAndVisitId(
            @PathVariable Long visitId){
        return visitService.getVisitById(visitId);
    }

    @PostMapping("/patient/add/visit")
    public @ResponseBody Visit addNewVisit(
            @RequestBody VisitJson visitJson
    ) {
        Visit visit = new Visit();

        visit.setDoctor(doctorService.getDoctor(visitJson.getDoctorId()));
        visit.setMedCard(medCardService.getMedCardById(visitJson.getMedCardId()));
        visit.setCauseOfTreatmentNote(visitJson.getCauseOfTreatmentNote());
        visit.setComplaints(visitJson.getComplaints());
        visit.setDiseaseAnamnesis(visitJson.getDiseaseAnamnesis());
        visit.setObjectiveData(visitJson.getObjectiveData());
        visit.setFillDate(visitJson.getDateOfVisit());
        visitService.addVisit(visit);
        return visit;
    }
}
