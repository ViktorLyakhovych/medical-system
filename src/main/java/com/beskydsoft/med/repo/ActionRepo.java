package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.Visit;
import com.beskydsoft.med.entity.medcard.history.Action;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionRepo extends CrudRepository<Action, Long> {

    List<Action> findAllByVisit(Visit visit);

}
