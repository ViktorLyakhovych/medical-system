package com.beskydsoft.med.entity.medcard;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LifeAnamnesis {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //дата заповнення
    @Column(name = "fillDate")
    private LocalDate fillDate;

    // місце народження
    @Column(name = "placeOfBirth")
    private String placeOfBirth;

    // особливості розвитку в дитячому віці
    @Column(name = "childhoodProgressSpecifics")
    private String childhoodProgressSpecifics;

    // перенесені захворювання
    @Column(name = "transmittedDiseases")
    private String transmittedDiseases;

    // шкідливі звички
    @Column(name = "badHabbits")
    private String badHabbits;

    // алергологічний аналіз
    @Column(name = "allergicAnalysis")
    private String allergicAnalysis;

    // епідеміологічний аналіз
    @Column(name = "epidemiologicalAnalysis")
    private String epidemiologicalAnalysis;

    // гінекологічний аналіз
    @Column(name = "gynecologicalAnalysis")
    private String gynecologicalAnalysis;

    // зворотній зв'язок на медичну карту
    @OneToOne(mappedBy = "lifeAnamnesis")
    @JsonBackReference
    private MedCard medCard;
}
