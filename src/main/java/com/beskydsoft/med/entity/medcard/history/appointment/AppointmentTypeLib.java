package com.beskydsoft.med.entity.medcard.history.appointment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentTypeLib {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Column(name = "appointmentType")
    private String appointmentType;

    @Column(name = "appointment_type_explanation")
    private String appointmentTypeExplanation;


}
