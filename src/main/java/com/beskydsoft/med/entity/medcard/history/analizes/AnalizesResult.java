package com.beskydsoft.med.entity.medcard.history.analizes;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AnalizesResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //дата заповнення (здачі аналізів)
    @Column(name = "date")
    private LocalDate date;

    // ім'я файлу з даними (фото)
    @Column(name = "dataFileName")
    private String dataFileName;

    // тип призначених аналізів
    @OneToOne(
                fetch = FetchType.EAGER
             )
    @JoinColumn(name = "analizesType_id")
    @JsonBackReference
    private AnalizesType analizesType;
}
