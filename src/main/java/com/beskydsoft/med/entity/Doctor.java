package com.beskydsoft.med.entity;

import com.beskydsoft.med.entity.data.gadget.UploadEvent;
import com.beskydsoft.med.entity.medcard.Visit;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Doctor extends User {

    @Column(name = "password")
    private String password;

    @Column(name = "tax_number") // індивідуальний податковий номер
    private String taxNumber;

    @Column(name = "specialization") // спеціалазація
    private String specialization;

    @Column(name = "license_number") // номер ліцензії
    private String licenseNumber;

    @Column(name = "license_start_date") // дата видачі ліцензії
    private LocalDate licenseStartDate;

    @Column(name = "work_experience_start") // стаж роботи (дата почату. стаж рахувати від неї
    private LocalDate workExperienceStart;

    @Column(name = "category") // категорія
    private String category;

    // завантажені(відправлені лікарю від пацієнта) файли
    @OneToMany
    @JsonManagedReference
    private Set<UploadEvent> uploadEvents;

    // зв'язок на пацієнтів лікаря
    @ManyToMany(mappedBy = "doctors")
    @JsonBackReference
    private Set<Patient> patients;

    // зворотній зв'язок на візит
    @OneToOne(mappedBy = "doctor")
    @JsonBackReference
    private Visit visit;

}
