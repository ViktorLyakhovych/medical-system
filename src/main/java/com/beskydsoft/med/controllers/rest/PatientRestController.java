package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PatientRestController {
    @Autowired
    PatientService patientService;

    @GetMapping("/rest/patient/{id}")
    public Patient getPatientById(@PathVariable Long id){
        return patientService.getPatient(id);
    }

    @GetMapping("/rest/patient/existence/{patientId}")
    public boolean isPatientExist(
            @PathVariable Long patientId)
//            @RequestParam(value = "patientId", required = true) Long patientId)
    {
        return patientService.isPatientExists(patientId);
    }








}
