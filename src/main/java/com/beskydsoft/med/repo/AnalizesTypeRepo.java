package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.history.analizes.AnalizesType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnalizesTypeRepo extends CrudRepository<AnalizesType, Long> {
}
