package com.beskydsoft.med.entity.data.gadget.uploads;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileEventsJson extends UploadDataRequest {

    public UploadedFile[] uploadFileJsonsArray;

}
