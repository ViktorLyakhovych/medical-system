package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.data.gadget.UploadedDataType;
import com.beskydsoft.med.repo.UploadDataTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UploadedDataTypeService {
    @Autowired
    UploadDataTypeRepo uploadDataTypeRepo;

    public UploadedDataType getUploadDataTypeByFileType(String fileType){
        return uploadDataTypeRepo.findDistinctFirstByUploadedDataTypeTitle(fileType);
    }

    public List<UploadedDataType> getAllUploadedDataTypes(){
        return (List<UploadedDataType>) uploadDataTypeRepo.findAll();
    }

    public UploadedDataType getUploadDataTypeByid(Long id){
        return uploadDataTypeRepo.findById(id).get();
    }

    public Long getIdByFileType(String fileType){
        UploadedDataType udt = uploadDataTypeRepo.findDistinctFirstByUploadedDataTypeTitle(fileType);
        return udt.getId();
    }

    public String getFileTypeById(Long id){
        UploadedDataType udt = uploadDataTypeRepo.findById(id).get();
        return udt.getUploadedDataTypeTitle();
    }
}
