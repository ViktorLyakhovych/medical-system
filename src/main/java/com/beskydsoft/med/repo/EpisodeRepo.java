package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.Episode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EpisodeRepo extends CrudRepository<Episode, Long> {
}
