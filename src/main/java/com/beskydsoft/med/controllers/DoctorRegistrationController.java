package com.beskydsoft.med.controllers;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.service.DoctorService;
import com.beskydsoft.med.service.UploadFileService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequestMapping("/registration/doctor")
public class DoctorRegistrationController {
    @Autowired
    private UploadFileService uploadFileService;

    @Autowired
    private DoctorService doctorService;

    @Value("${upload.path.photos}")
    private String uploadPathPhotos;

    @GetMapping
//            (value = "/registration/doctor")
    public String getIndex() {
        return "registrationDoctor";
    }

    private static final Logger log = Logger.getLogger(DoctorRegistrationController.class);

//    @PostMapping(path = "/add")
//    public String addDoctor(
//            String lastname,
//            String firstname,
//            String secondname,
//            String phone,
//            String email,
//            String password,
//            String spec) {
//        return "redirect:/patients";
//    }

    // registration controller
    @PostMapping("/add")
    public String addDoctor(
            @RequestParam("file") MultipartFile file,
            @ModelAttribute Doctor doctor, Model model
    ) throws IOException {
        model.addAttribute("doctor", doctor);
        String fileName = uploadFileService.uploadFile(file, uploadPathPhotos);
        doctor.setPhotoFileName(fileName);
        doctorService.addDoctor(doctor);
        return "redirect:/patients";
    }
}
