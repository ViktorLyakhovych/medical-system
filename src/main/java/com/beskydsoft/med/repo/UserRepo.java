package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User, Long> {
   // User findByUsername(String username);
}
