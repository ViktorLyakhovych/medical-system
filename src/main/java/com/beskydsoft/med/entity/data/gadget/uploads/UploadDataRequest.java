package com.beskydsoft.med.entity.data.gadget.uploads;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class UploadDataRequest {

    private Long doctorId;
    private Long patientId;
    private String description;
    private String measureDateTime;


}
