package com.beskydsoft.med.entity.data.gadget;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UploadedDataType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uploaded_data_type_title")
    private String uploadedDataTypeTitle;

    @Column(name = "uploaded_data_type_explanation")
    private String uploadedDataTypeExplanation;

}
