package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.data.gadget.uploads.UploadFileEvent;
import com.beskydsoft.med.repo.UploadFileEventRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UploadFileEventService {
    @Autowired
    UploadFileEventRepo uploadFileEventRepo;

    public void addUploadFileEvent(UploadFileEvent uploadFileEvent){
        uploadFileEventRepo.save(uploadFileEvent);
    }

    public UploadFileEvent getUploadFileEventById(Long id){
        return uploadFileEventRepo.findById(id).get();
    }
}
