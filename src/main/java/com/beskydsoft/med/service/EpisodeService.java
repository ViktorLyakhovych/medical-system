package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.medcard.Episode;
import com.beskydsoft.med.repo.EpisodeRepo;
import com.beskydsoft.med.repo.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EpisodeService {
    @Autowired
    EpisodeRepo episodeRepo;
    @Autowired
    PatientRepo patientRepo;


    public Iterable<Episode> getAllEpisodesByPatientId(Long id){
        Patient p = patientRepo.findById(id).get();
        return p.getMedCard().getEpisodes();
    }

    public Iterable<Episode> getAllEpisodesByPatient(Patient patient){
//        Patient p = patientRepo.findById(id).get();
        return patient.getMedCard().getEpisodes();
    }

    public Episode getEpisodeByPatientId(Long patientId, Long episodeId){
        Iterable<Episode> allEpisodes = getAllEpisodesByPatientId(patientId);
        for (Episode episode: allEpisodes) {
            if (episode.getId().equals(episodeId)) return episode;
        }
        return null;
    }

    public Episode getEpisodeById(Long episodeId){
        return episodeRepo.findById(episodeId).get();
    }
}
