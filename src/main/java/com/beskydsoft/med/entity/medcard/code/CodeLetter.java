package com.beskydsoft.med.entity.medcard.code;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CodeLetter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //назва секції
    @Column(name = "title")
    private String title;

    //буква секції
    @Column(name = "letter")
    private char letter;

    //зв. зв'язок з кодами
    @OneToMany(fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<Code> codes;
}
