package com.beskydsoft.med.controllers;

import com.beskydsoft.med.entity.log.activity.ActivityEvent;
import com.beskydsoft.med.entity.log.activity.LogEvent;
import com.beskydsoft.med.service.ActivityEventService;
import com.beskydsoft.med.service.LogEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/android")
public class PatientActivityController  {
    @Autowired
    private LogEventService logEventService;
    @Autowired
    private ActivityEventService activityEventService;

    @GetMapping("/patient/{patientId}/set/activity/{actEventId}")
    public LogEvent SetActivity(
            @PathVariable Long patientId,
            @PathVariable Long actEventId
            ) {
        LogEvent logEvent = logEventService.createNewLogEvent(actEventId, patientId);
        logEventService.saveLogEvent(logEvent);
        return logEvent;
    }

    @GetMapping("/get/logevents/all")
    public Iterable<LogEvent> getAllLogEvents(){
        return logEventService.getAllLogEvents();
    }

    @GetMapping("/get/logevents/patient/{patientId}")
    public Iterable<LogEvent> getAllLogEvents(
            @PathVariable Long patientId
    ){
        return logEventService.getAllLogEventsByPatient(patientId);
    }

    @GetMapping("/get/activities/string")
    public List<String> getAllStringActivityTypes(){
        List<ActivityEvent> ae = new ArrayList<>();
        activityEventService.getAll().forEach(ae::add);
        List<String> stringActivityTypes = new ArrayList<>();
        for (ActivityEvent a: ae) {
            stringActivityTypes.add(a.getEventDescription());
        }
        return stringActivityTypes;
    }

    @GetMapping("/get/activities")
    public List<ActivityEvent> getAllActivityTypes(){
        List<ActivityEvent> ae = new ArrayList<>();
        activityEventService.getAll().forEach(ae::add);
        return ae;
    }
}
