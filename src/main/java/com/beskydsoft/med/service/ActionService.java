package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.medcard.history.Action;
import com.beskydsoft.med.repo.ActionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActionService {
    @Autowired
    ActionRepo actionRepo;
    @Autowired
    VisitService visitService;


    public List<Action> getAllbyVisitId(Long visitId){
        return actionRepo.findAllByVisit(visitService.getVisitById(visitId));
    }

    public Action getActionById (Long actionId){
        return actionRepo.findById(actionId).get();
    }

    public void addAction(Action action){
        actionRepo.save(action);
    }
}
