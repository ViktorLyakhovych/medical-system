package com.beskydsoft.med.entity.medcard;

import com.beskydsoft.med.entity.medcard.episode.BodyResearch;
import com.beskydsoft.med.entity.medcard.episode.CommonStatus;
import com.beskydsoft.med.entity.medcard.history.Diagnosis;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Episode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // зворотній зв'язок на медичну карту
    @ManyToOne(
            fetch = FetchType.LAZY)
            //cascade = CascadeType.ALL)
    @JsonBackReference
    private MedCard medCard;

    // BodyResearch
    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "episode_id")
    private Set<BodyResearch> bodyResearches = new HashSet<>();

    // Загальний стан пацієнта
    @OneToOne(
//            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "common_status_id")
    private CommonStatus commonStatus;

    // візити (do not set EAGER! because it fails)
    @ManyToMany(
            fetch = FetchType.LAZY)
    @JoinTable(name = "episode_visits",
            joinColumns = @JoinColumn(name = "episode_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "visit_id", referencedColumnName = "id"))
    private Set<Visit> visits = new HashSet<>();

    // діагноз
    @OneToOne(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private Diagnosis diagnosis;
}
