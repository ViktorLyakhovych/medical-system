package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.log.activity.ActivityEvent;
import com.beskydsoft.med.repo.ActivityEventRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityEventService {
    @Autowired
    ActivityEventRepo activityEventRepo;

    public ActivityEvent getActivityEventByName(String name){
        return activityEventRepo.findDistinctByEventDescription(name);
    }

    public ActivityEvent getActivityEventById(Long id){
        return activityEventRepo.findById(id).get();
    }

    public Iterable<ActivityEvent> getAll(){
        return activityEventRepo.findAll();
    }
}
