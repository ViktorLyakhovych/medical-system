var wavesurfer = WaveSurfer.create({
    container: '#waveform'
});

wavesurfer.load('/sound/heart-beat.ogg');

wavesurfer.on('ready', function () {
    wavesurfer.play();
});