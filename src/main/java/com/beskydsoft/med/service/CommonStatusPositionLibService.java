package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.medcard.episode.CommonStatusPositionLib;
import com.beskydsoft.med.repo.CommonStatusPositionLibRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonStatusPositionLibService {
    @Autowired
    CommonStatusPositionLibRepo commonStatusPositionLibRepo;

    public CommonStatusPositionLib getPatientStatusById(Long id){
        return commonStatusPositionLibRepo.findById(id).get();
    }
}
