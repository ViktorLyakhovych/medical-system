package com.beskydsoft.med.controllers;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.json.CommonResponse;
import com.beskydsoft.med.json.LoginRequest;
import com.beskydsoft.med.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;
    //@Autowired
    //private SecurityContextRepository repository;
    //@Autowired
    // RememberMeServices rememberMeServices;
    @Autowired
    private DoctorService dotorService;

    @GetMapping(value = "/login")
    public String getLogin(Model model) {
        return "login";
    }

    @PostMapping(value = "/login")
    @ResponseBody
    public Object performLogin(
            @RequestBody LoginRequest json,
            HttpServletRequest request,
            HttpServletResponse response) {
        Doctor doctor = dotorService.findByCredentials(json.getEmail(), json.getPassword());
        if (doctor == null) {
            CommonResponse responseJson = new CommonResponse();
            responseJson.setErrorDescription("Не правильний логін/пароль");
            response.setStatus(401);
            return responseJson;
        }
        UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken(
                        json.getEmail(),
                        json.getPassword());
        Authentication auth = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(auth);
        //repository.saveContext(SecurityContextHolder.getContext(), request, response);
        //rememberMeServices.loginSuccess(request, response, auth);
        return new CommonResponse();
    }
}
