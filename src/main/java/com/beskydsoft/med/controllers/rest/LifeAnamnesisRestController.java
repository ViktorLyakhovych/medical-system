package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.medcard.LifeAnamnesis;
import com.beskydsoft.med.service.LifeAnamnesisService;
import com.beskydsoft.med.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LifeAnamnesisRestController {
    @Autowired
    LifeAnamnesisService lifeAnamnesisService;
    @Autowired
    PatientService patientService;

    @GetMapping("/lifeAnamnesis/patient={patientId}")
    public LifeAnamnesis getALifeAnamnesisByPatientId(@PathVariable Long patientId){
        return patientService.getPatient(patientId).getMedCard().getLifeAnamnesis();
    }
}
