package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.medcard.code.CodeColor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeColorRepo extends CrudRepository<CodeColor, Long> {
}
