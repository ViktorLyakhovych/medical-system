package com.beskydsoft.med.controllers.rest;

import com.beskydsoft.med.entity.medcard.Passport;
import com.beskydsoft.med.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PassportRestController {
    @Autowired
    PatientService patientService;

    @GetMapping("/passport/patient={patientId}")
    public Passport getAPassportByPatientId(@PathVariable Long patientId){
        return patientService.getPatient(patientId).getMedCard().getPassport();
    }
}
