INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (1, 'Кардіолог', 'petro.nykodymenko@ukr.net', 'Петро', 'Никодименко', '380971234567', null, 'Іванович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (2, 'Проктолог', 'vasyl.shkidchenko@ukr.net', 'Василь', 'Шкідченко', '380661112233', null, 'Олексійович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (3, 'Невропатолог', 'yavorsky@ukr.net', 'Ігор', 'Яворський', '380689998877', null, 'Васильович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (4, 'інвалід ІІ групи. Періодичні головні болі в ділянці потилиці, що пов’язує з підвищенним тиском', 'bilozub@ukr.net', 'Олег', 'Білозуб', '380509519551', 'photo04.jpg', 'Васильович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (5, 'УБД. Задишка після фізичного навантаження', 'buryachenko.a@email.ua', 'Анатолій', 'Буряченко', '380957008090', 'photo05.jpg', 'Степанович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (6, 'Відмічається біль за грудиною стискуючого характеру який іррадіює в ліву руку', 'mariya@gmail.com', 'Марія', 'Нещадименко', '380979786431', 'photo06.jpg', 'Олександрівна');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (7, 'Незначні набряки на дистальній частині гомілок', 'nokodim@gmail.com', 'Никодим', 'Нехворійнога', '380966548721', 'photo07.jpg', 'Джонович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (8, 'постінфарктний кардіосклероз (від січня 1997 року, поразка задньої стінки лівого шлуночка), прогресуюча стенокардія напруги, синусова аритмія', 'nehodyvlis@gmail.com', 'Василь', 'Неходивліс', '380951236598', 'photo08.jpg', 'Афанасійович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (9, 'Періодичні , помірної інтенсивності, недовготривалі головні болі ,локалізовані в потиличній ділянці', 'kryvoshapka@gmail.com', 'Максим', 'Кривошапка', '380965558899', 'photo09.jpg', 'Батькович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (10, 'Тупий тиснучий головний біль, без чіткої локалізації. (нагадати про оплату за минулий місяць)', 'shpulman@gmail.com', 'Мойша', 'Шпульман', '380501232312', 'photo10.jpg', 'Ісаакович');
INSERT INTO monimed.user (id, additional_info, email, first_name, last_name, phone_number, photo_file_name, second_name) VALUES (11, 'Інтоксикаційний синдром', 'duminkevych@mail.ua', 'Ринат', 'Думінкевич', '380686542187', 'photo11.jpg', 'Іванович');

INSERT INTO monimed.doctor (category, license_number, license_start_date, password, specialization, tax_number, work_experience_start, id) VALUES ('3', 'АЕ459837', '2010-07-01', '123', 'проктолог', '1112223334', '2010-07-02', 1);
INSERT INTO monimed.doctor (category, license_number, license_start_date, password, specialization, tax_number, work_experience_start, id) VALUES ('1', 'АГ598320 ', '2014-06-10', 'qwe', 'травматолог', '2223334445', '2014-06-11', 2);
INSERT INTO monimed.doctor (category, license_number, license_start_date, password, specialization, tax_number, work_experience_start, id) VALUES ('2', 'АЕ571038', '2000-08-22', 'asd', 'ЛОР', '3334445556', '2000-08-23', 3);

INSERT INTO monimed.common_status_position_lib (id, common_status_position) VALUES (1, 'ACTIVE');
INSERT INTO monimed.common_status_position_lib (id, common_status_position) VALUES (2, 'PASSIVE');
INSERT INTO monimed.common_status_position_lib (id, common_status_position) VALUES (3, 'FORCED');

INSERT INTO monimed.analizes_type (id, title, type_explanation) VALUES (1, 'BLOOD_BIOCHEMISTRY', 'Біохімія крові');
INSERT INTO monimed.analizes_type (id, title, type_explanation) VALUES (2, 'LIPIDOGRAM', 'Ліпідограма');
INSERT INTO monimed.analizes_type (id, title, type_explanation) VALUES (3, 'BLOOD_GEN_TEST', 'Загальний аналіз крові');
INSERT INTO monimed.analizes_type (id, title, type_explanation) VALUES (4, 'COAGULOGRAM', 'Коагулограма');
INSERT INTO monimed.analizes_type (id, title, type_explanation) VALUES (5, 'URINE_GEN_TEST', 'Загальний аналіз сечі');

INSERT INTO monimed.appointment_type_lib (id, appointment_type, appointment_type_explanation) VALUES (1, 'PILLS', 'таблетки');
INSERT INTO monimed.appointment_type_lib (id, appointment_type, appointment_type_explanation) VALUES (2, 'TO_VEIN', 'у вену');
INSERT INTO monimed.appointment_type_lib (id, appointment_type, appointment_type_explanation) VALUES (3, 'TO_MUSCLE', 'в м''язи');
INSERT INTO monimed.appointment_type_lib (id, appointment_type, appointment_type_explanation) VALUES (4, 'PROCEDURE', 'процедура');
INSERT INTO monimed.appointment_type_lib (id, appointment_type, appointment_type_explanation) VALUES (5, 'SYRUP', 'сироп');
INSERT INTO monimed.appointment_type_lib (id, appointment_type, appointment_type_explanation) VALUES (6, 'DROPS', 'каплі');
INSERT INTO monimed.appointment_type_lib (id, appointment_type, appointment_type_explanation) VALUES (7, 'OINTMENT', 'мазь');

INSERT INTO monimed.activity_event (id, description) VALUES (1, 'LOGIN');
INSERT INTO monimed.activity_event (id, description) VALUES (2, 'UPLOAD');
INSERT INTO monimed.activity_event (id, description) VALUES (3, 'VIEW');

INSERT INTO monimed.passport (id, accommodation, education, family_members_sick, fill_date, injuries, invalidity, marital_status, operations, prison, sex) VALUES (1, 'задовільні', 'вища освіта', 'У членів сім''ї захворювання відсутні', '1914-07-28', 'Травми відсутні', 'інвалідність відсутня', 'одружений', 'Вирізано апендикс в молодшому шкільному віці', 'активна', 'чол.');
INSERT INTO monimed.passport (id, accommodation, education, family_members_sick, fill_date, injuries, invalidity, marital_status, operations, prison, sex) VALUES (2, 'нормальні', 'середня освіта', 'Жінка корова діти свині', '1948-08-29', 'Травма коліна', 'інвалідність. Коліно', 'неодружений', 'Операція на черевній грижі', 'неактивна', 'чол.');

INSERT INTO monimed.life_anamnesis (id, allergic_analysis, bad_habbits, childhood_progress_specifics, epidemiological_analysis, fill_date, gynecological_analysis, place_of_birth, transmitted_diseases) VALUES (1, 'алергічна реакція на пух тополі', 'паління з 15років', 'особливості відсутні', '-', '1914-07-28', '-', 'c.Кукавка, Вінницька обл.', 'краснуха');
INSERT INTO monimed.life_anamnesis (id, allergic_analysis, bad_habbits, childhood_progress_specifics, epidemiological_analysis, fill_date, gynecological_analysis, place_of_birth, transmitted_diseases) VALUES (2, 'алергічна реакція на пилок', 'не палить', 'особливості відсутні', '-', '1948-08-29', '-', 'c.Сліди, Житомирська обл.', 'жовтуха');

INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (1, 1, 1);
INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (2, 2, 2);
INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (3, null, null);
INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (4, null, null);
INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (5, null, null);
INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (6, null, null);
INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (7, null, null);
INSERT INTO monimed.med_card (id, life_anamnesis_id, passport_id) VALUES (8, null, null);

INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1980-01-28', '380509519552', 'HR', null, 'м.Калуш, вул.Будівельників, 158/695', 4, 1);
INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1970-02-28', '380957008091', 'QA', null, 'м.Калуш, вул.Польова, 1991', 5, 2);
INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1960-03-28', '380979786432', 'BA', null, 'с.Боднарів, вул.Івано-Франківська, 444', 6, 3);
INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1950-04-28', '380966548722', 'DevOps', null, 'с.Підмихайля, вул.Завійська, 222', 7, 4);
INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1949-05-28', '380951236599', 'Developer', null, 'с.Добровляни, вул.Сонячна, 33а', 8, 5);
INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1948-06-28', '380965558900', 'Слюсар Іго розряду', null, 'с.Підмихайля, вул.Степова, 444', 9, 6);
INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1947-07-28', '380501232313', 'Зварщик ІIго розряду', null, 'с.Боднарів, вул.І.Франка, 222', 10, 7);
INSERT INTO monimed.patient (birthday, phone_number_reserve, profession, registration_date, residence_address, id, med_card_id) VALUES ('1946-08-28', '380686542188', 'тракторист', null, 'м.Калуш, вул.Довга, 777', 11, 8);

INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (4, 1);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (5, 1);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (7, 1);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (8, 1);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (4, 2);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (6, 2);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (7, 2);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (9, 2);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (5, 3);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (6, 3);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (7, 3);
INSERT INTO monimed.patient_doctors (patient_id, doctor_id) VALUES (10, 3);

INSERT INTO monimed.visit (id, cause_of_treatment_note, complaints, disease_anamnesis, fill_date, objective_data, doctor_id, med_card_id)
VALUES (1, 'До весілля загоїться', 'Тупий біль в лівій гомілці правої руки над підборіддям', 'Якийсь анамнез першого пацієнта. Не знаю навіть що то таке', '1914-07-28', 'Обє''ктивно, пацієнт здоровий, просто косить від роботи', 1, 1);
INSERT INTO monimed.visit (id, cause_of_treatment_note, complaints, disease_anamnesis, fill_date, objective_data, doctor_id, med_card_id)
VALUES (2, 'Одужав (майже)', 'Тупий біль в правому коліні лівої руки над підборіддям', 'Якийсь анамнез другого пацієнта. Не знаю навіть що то є', '1941-08-20', 'Обє''ктивно, пацієнт здоровий. Майже', 3, 2);

INSERT INTO monimed.diagnosis (id, diagnosis_note, codes_id) VALUES (1, 'Лімфаделіт гострий', 4);
INSERT INTO monimed.diagnosis (id, diagnosis_note, codes_id) VALUES (2, 'Дерматит / атопічна екзема', 5);

INSERT INTO monimed.common_status (id, consciousness, fill_date, height, mucous_assessment, skin_assessment, state, stature, weight, common_status_position) VALUES (1, 'свідомість чиста', '2018-03-03', '175 см', 'На поверхні зовнішньої мембрани містяться капсулоподібний слизовий покрив і мікрокапсула', 'щільний шкірний покрив вкритий роговими щитками', 'газоподібний', 'ектоморф', '78 кг', 1);
INSERT INTO monimed.common_status (id, consciousness, fill_date, height, mucous_assessment, skin_assessment, state, stature, weight, common_status_position) VALUES (2, 'свідомість мутна', '2019-07-07', '180', 'тест1', 'все ок', 'рідкий', 'атлетична', '82 кг', 1);

INSERT INTO monimed.episode (id, common_status_id, diagnosis_id, med_card_id) VALUES (1, 1, 1, 1);
INSERT INTO monimed.episode (id, common_status_id, diagnosis_id, med_card_id) VALUES (2, 2, 2, 1);

INSERT INTO monimed.action (id, visit_id) VALUES (1, 1);
INSERT INTO monimed.action (id, visit_id) VALUES (2, 2);

INSERT INTO monimed.analizes_result (id, data_file_name, date, analizes_type_id) VALUES (1, null, '2019-07-26', 2);
INSERT INTO monimed.analizes_result (id, data_file_name, date, analizes_type_id) VALUES (2, null, '2019-07-26', 1);

INSERT INTO monimed.analizes_assignment (id, fill_date, action_id, analizes_result_id, analizes_type_id) VALUES (1, '1914-08-02', 1, 1, 2);
INSERT INTO monimed.analizes_assignment (id, fill_date, action_id, analizes_result_id, analizes_type_id) VALUES (2, '1942-04-29', 2, 2, 1);

INSERT INTO monimed.appointment (id, consumption_аmount, consumption_сount, consumption_days, date, duration, prescription, title, appointment_type, action_id)
VALUES (1, '0,5 таблетки', 3, 84, '2019-07-22', 2, 'Від артеріальної гіпертензії', 'Каптопрес', 1, 1);
INSERT INTO monimed.appointment (id, consumption_аmount, consumption_сount, consumption_days, date, duration, prescription, title, appointment_type, action_id)
VALUES (2, '2 таблетки', 3, 84, '2019-07-22', 1, 'Від аритмії серця', 'Корвалол', 2, 1);
INSERT INTO monimed.appointment (id, consumption_аmount, consumption_сount, consumption_days, date, duration, prescription, title, appointment_type, action_id)
VALUES (3, '1 таблеткa', 3, 84, '2019-07-22', 1, 'Для серця і судин', 'КардіоАктив Таурин', 3, 1);
INSERT INTO monimed.appointment (id, consumption_аmount, consumption_сount, consumption_days, date, duration, prescription, title, appointment_type, action_id)
VALUES (4, '1 таблеткa', 3, 84, '2019-07-23', 1, 'Для серця і судин', 'КардіоАктив Таурин', 3, 2);

INSERT INTO monimed.body_research (id, cardiovascular_system, digestive_system, endocrine_system, fill_date, genitourinary_system, peripheral_edema, respiratory_system, episode_id) VALUES (1, 'Стан серцево-судинної системи задовільний', 'Стан травної системи незадовільний, потребує ододаткової консультації', 'Ендокринна система задовільна', '2018-03-04', 'Сечостатева система', true, 'Дихальна система. Задовільно', 1);

INSERT INTO monimed.episode_visits (episode_id, visit_id) VALUES (1, 1);
INSERT INTO monimed.episode_visits (episode_id, visit_id) VALUES (2, 1);

