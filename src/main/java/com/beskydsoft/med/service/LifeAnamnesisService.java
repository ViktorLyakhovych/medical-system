package com.beskydsoft.med.service;

import com.beskydsoft.med.entity.Patient;
import com.beskydsoft.med.entity.medcard.LifeAnamnesis;
import com.beskydsoft.med.repo.LifeAnamnesisRepo;
import com.beskydsoft.med.repo.MedCardRepo;
import com.beskydsoft.med.repo.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LifeAnamnesisService {
    @Autowired
    LifeAnamnesisRepo lifeAnamnesisRepo;
    @Autowired
    MedCardRepo medCardRepo;
    @Autowired
    PatientRepo patientRepo;

    /**
     * Gets life anamnesis of patient
     * @param patientId id of desired patient
     * @return lifeAnamnesis
     */
     public LifeAnamnesis getLifeAnamnesisByPatientId(Long patientId){
         Optional<Patient> p = patientRepo.findById(patientId);
         return p.map(patient -> patient.getMedCard().getLifeAnamnesis()).orElse(null);
     }

}
