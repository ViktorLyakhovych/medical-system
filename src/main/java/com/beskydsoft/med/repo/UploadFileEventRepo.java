package com.beskydsoft.med.repo;

import com.beskydsoft.med.entity.data.gadget.uploads.UploadFileEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadFileEventRepo extends CrudRepository<UploadFileEvent, Long> {

}
