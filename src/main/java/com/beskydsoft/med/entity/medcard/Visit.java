package com.beskydsoft.med.entity.medcard;

import com.beskydsoft.med.entity.Doctor;
import com.beskydsoft.med.entity.medcard.code.Code;
import com.beskydsoft.med.entity.medcard.history.Action;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // епізоди, по яких було звернення
    @ManyToMany(mappedBy = "visits")
    @JsonBackReference
    private Set<Episode> episodes = new HashSet<>();

    // лікар, до якого зверталися
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    //дата звернення
    @Column(name = "fillDate")
    private LocalDate fillDate;

    //коди причин звернення
//    @ManyToMany(
//            fetch = FetchType.EAGER)
//    @JoinColumn(name = "visit_id")
//    private Set<Code> codes = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "visit_codes",
            joinColumns = @JoinColumn(name = "visit_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "code_id", referencedColumnName = "id"))
    private Set<Code> codes;

    //примітка причини звернення
    @Column(name = "causeOfTreatmentNote")
    private String causeOfTreatmentNote;

    //скарги
    @Column(name = "complaints")
    private String complaints;

    //анамнез хвороби
    @Column(name = "diseaseAnamnesis")
    private String diseaseAnamnesis;

    //об'єктивні дані
    @Column(name = "objectiveData")
    private String objectiveData;

    @ManyToOne
            //(fetch = FetchType.LAZY)
    @JsonBackReference
    private MedCard medCard;

    // дії по зверненню
    @OneToMany(
        cascade = CascadeType.ALL)
    @JoinColumn(name = "visit_id")
    private Set<Action> actions = new HashSet<>();
}
