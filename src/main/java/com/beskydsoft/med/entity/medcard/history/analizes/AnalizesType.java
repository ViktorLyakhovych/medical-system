package com.beskydsoft.med.entity.medcard.history.analizes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AnalizesType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    String title;

    @Column(name = "type_explanation")
    private String typeExplanation;

//    // зворотній лінк на призначення аналізів
//    @OneToOne(fetch = FetchType.LAZY,
//            mappedBy = "analizesType")
//    @JsonBackReference
//    private AnalizesAssignment analizesAssignment;
//
//    // зворотній лінк на результати аналізів
//    @OneToOne(fetch = FetchType.LAZY,
//            mappedBy = "analizesType")
//    @JsonBackReference
//    private AnalizesResult analizesResult;
}
