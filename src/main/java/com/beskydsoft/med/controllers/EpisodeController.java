package com.beskydsoft.med.controllers;

import com.beskydsoft.med.entity.medcard.Episode;
import com.beskydsoft.med.service.DoctorService;
import com.beskydsoft.med.service.EpisodeService;
import com.beskydsoft.med.service.PatientService;
import com.beskydsoft.med.service.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class EpisodeController {
    @Autowired
    PatientService patientService;

    @Autowired
    DoctorService doctorService;

    @Autowired
    EpisodeService episodeService;

    @Autowired
    VisitService visitService;

    @GetMapping("/patient/{patientId}/episode/{episodeId}")
    public String getVisit(
            @PathVariable Long episodeId,
            @PathVariable Long patientId,
            Model model){
        Episode episode = episodeService.getEpisodeById(episodeId);
        model.addAttribute("episode", episode);
        model.addAttribute("visits", episode.getVisits());
        model.addAttribute("patient", patientService.getPatient(patientId));
        model.addAttribute("patients", patientService.getAll());
        model.addAttribute("doctors", doctorService.getDoctorsByPatientId(patientId));
        return "episode";
    }
}
